GNOME Connections features an onboard dialog which presents some illustrations explaining the app's functionality.

The onboarding dialog is meant to run **only** when the user launches the app for the first time.

To forcibly make Connections show the onboarding dialog you need to run:

```gsettings set org.gnome.Connections first-run true```

and re-launch the application.

In a **Flatpak** version you need to run the command above inside the container. For that, firstly enter a bash session inside the container with `flatpak --devel --command=bash run org.gnome.Connections` and use the same command of the step above.

> If you are using the development branch of Connections (built from the source with Flatpak in GNOME Builder or obtained from the GNOME Nightly app Flatpak repository), the application ID will be `org.gnome.Connections.Devel` instead.

* The `first-run` key is checked at GApplication `activate` event. See https://gitlab.gnome.org/GNOME/connections/-/blob/master/src/application.vala#L113