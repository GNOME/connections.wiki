# Welcome to Connections

A remote desktop client for the GNOME desktop environment.

This is the developer documentation. For user documentation visit https://gitlab.gnome.org/GNOME/connections/-/tree/master/help

## Overview

Connections is designed to split remote-connection specifics from the frontend. For this reason it is expected that most of the protocol specific work is done in the remote connection display widget libraries such as [Gtk-Vnc](https://gitlab.gnome.org/GNOME/gtk-vnc) and [Gtk-Frdp](https://gitlab.gnome.org/GNOME/gtk-frdp/). Making this repository mostly for the app/frontend logic.

The `VncConnection` and `RdpConnection` classes are derived from the `Connection` abstract class, which implements some of the functionality that applies to all protocols and relies heavily on its properties being serializable. This way, the `Database` singleton object manages the collection of remote connections data by serializing a `Connection` object's [properties to/from a Keyfile](https://gitlab.gnome.org/GNOME/connections/-/blob/master/src/database.vala#L154). This mechanism allows for `Connection` properties to be always in sync with the disk.

`VncPreferencesWindow` and `RdpPreferencesWindow` derive from the `PreferencesWindow` class. These dialogs basically bind the `Connection` properties to the widgets that allow for editing them. Edits get automatically synchronized with their respective `Connection` object and consequently saved to disk.

## Other guides

- [Building instructions](https://gitlab.gnome.org/GNOME/connections/-/wikis/Building-instructions)
- [Testing the Onboarding Dialog](https://gitlab.gnome.org/GNOME/connections/-/wikis/Testing-the-onboarding-dialog)